//'use strict';

const uuid = require('uuid/v4');
const mysql = require('mysql2/promise');

const userData = require('../sampleData/userData');

module.exports = {
  populate: async ({ target, limit }) => {

    const amount = parseInt(limit);

    const databaseMigration = await mysql.createConnection({
      host: 'localhost',
      user: 'root',
      database: 'db_mig'
    });

    console.log('Created connection!');

    const existedTableContent = await databaseMigration.query(`SHOW TABLES LIKE 'Users'`);

    console.log(`Checked available table : Is users available? ${existedTableContent[0] !== undefined}`);

    if (existedTableContent[0] !== undefined) {
      await databaseMigration.query(`DROP TABLE Users`);

      console.log('Dropped table!');
    }

    await databaseMigration.query(
      `CREATE TABLE Users (
        id INT AUTO_INCREMENT,
        uuid VARCHAR(25),
        name VARCHAR(25),
        age INT,
        created_at TIMESTAMP default CURRENT_TIMESTAMP,
        salary FLOAT,
        skill_level VARCHAR(25),
        address VARCHAR(255),
        PRIMARY KEY (id)
    )`);

    console.log('Created table!');

    for (let count = 0; count < amount; count++) {

      const createdData = {
        uuid: uuid(),
        name: userData.name[Math.floor(Math.random() * userData.name.length)],
        age: userData.age[Math.floor(Math.random() * userData.age.length)],
        salary: userData.salary[Math.floor(Math.random() * userData.salary.length)],
        skill_level: userData.skill_level[Math.floor(Math.random() * userData.skill_level.length)],
        address: userData.address[Math.floor(Math.random() * userData.address.length)]
      };

      await databaseMigration.query(
        'INSERT INTO Users (uuid, name, age, salary, skill_level, address) VALUES (?,?,?,?,?,?)',
        [
          createdData.uuid,
          createdData.name,
          createdData.age,
          createdData.salary,
          createdData.skill_level,
          createdData.address
        ]);

        process.stdout.write("\r\x1b[K")
        process.stdout.write(`Data created (${count + 1}/${amount})`);
    }

    console.log(`\nPopulated ${amount} data into Users table!`);

    await databaseMigration.end();

    console.log('Connection was closed!');
  }
};
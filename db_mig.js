'use strict'

const DatabaseManager = require('./managers/DatabaseManager');

const command = process.argv[2].replace('-','');
let parameters = {};

for (const parameter of process.argv.slice(3, process.argv.length)) {
    const contents = parameter.replace('-','');   
    const splitedContents = contents.split('=');
    
    const key = splitedContents[0];
    const value = splitedContents[1];

    parameters[key] = value;
}

const cmdInput = {
    command,
    parameters
};

DatabaseManager[cmdInput.command](cmdInput.parameters);
"use strict";

module.exports = {
  name: ["Sam", "Rex", "John", "Tim", "Momo"],
  age: [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
  salary: [15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000],
  skill_level: ["Beginner", "Intermediate", "Expert"],
  address: ["A", "B", "C", "D"]
};
